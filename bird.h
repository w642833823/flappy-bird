#ifndef __BIRD_H__
#define __BIRD_H__

void BirdInit(void);

void BirdStart(void);

int BirdLoop(void);

void BirdDrawScreen(void);

void BirdTest(void);

void BirdFlap(void);

void GameOver(void);

extern int isStarted;

#endif